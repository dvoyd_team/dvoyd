<?php

require(__DIR__.'/init.php');

/**
* DvoydSolrium class
*/
class DvoydSolrium 
{
    private $client;
    private $query;
    private $resultset;
    public $data;
    private $obj;
    private $select;
    private $grouping;
    private $default = false;

    
    function __construct($config)
    {
        $this->obj = new stdClass();
        $this->client =  new Solarium\Client($config);
        $this->data = "";
    }

    public function setDvoydOptions($select){
      $this->select = $select;
        // set a query (all prices starting from 12)
        $this->query = $this->client->createSelect( $this->select); 
        $this->grouping =  $this->query->getGrouping();
        $this->grouping->addField("domaine");
        // maximum number of items per group
        $this->grouping->setLimit(10);
        // get a group count
        $this->grouping->setNumberOfGroups(true);

       
        //$this->query->addSort('dprice', $this->query::SORT_ASC);

    }


public function setDefault($select){
      $this->select = $select;
      $this->default = true;
      $this->query = $this->client->createSelect( $this->select); 
}

    public function executesQuery(){
        try {
            $this->resultset = $this->client->select($this->query);

        } catch (Exception $e) {
            
        }
      
    }
    

    public function setView(){
        $unique = array();
        if ($this->default) {
          foreach ($this->resultset as  $v) {
             $array = [
               'name' => $v->name,
               'price' => $v->price,
               'image' => $v->image,
               'domaine' => $v->domaine,
               'link' => $v->link,
               'description' => $v->name
              ];
              $unique[] = $array;
          
           }
        }else{
         $group = $this->resultset->getGrouping();
         foreach ($group as  $groupkey => $groupField) {
          foreach ($groupField as $valueGroup) {
            foreach ($valueGroup as $v) {
              //echo $v->name;
                $array = [
               'name' => $v->name,
               'price' => $v->price,
               'image' => $v->image,
               'domaine' => $v->domaine,
               'link' => $v->link,
               'description' => $v->name
              ];
              $unique[] = $array;
            }
          }
          
        }
       
        }
        
        echo json_encode(array_values($unique));
    }

    public function getData(){
            
        echo $this->data;
    }
}
 

$solrium = new DvoydSolrium($config);



?>