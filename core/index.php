<?php
include_once 'solrium.php';

$q = "*:*";
$limit = 9;
$start = 0;
$state = false;

if (isset($_GET) && !empty($_GET)) {
	if (isset($_GET['q']))
		$q = $_GET['q'];

	if (isset($_GET['limit']))
	    $limit = $_GET['limit'];

	if (isset($_GET['start']))
	{
	    $start = $_GET['start'];
	    $state = true;
	}


//query

	$select = [
               'query'              => htmlspecialchars(trim($q)),
                'wt'                => "php",
                'fields'            => array('name','price','link','image','domaine','dprice'),
                'sort'              => array("price" => "asc"),
                'start'  		      	=> $start,
                'rows'              => $limit
             ];
    if ($state) {
    	# code...
    	$solrium->setDefault($select);
    }else{
    	$solrium->setDvoydOptions($select);
    }
   //$solrium->setDvoydOptions($select);
   $solrium->executesQuery(); 
   $solrium->setView(); 


    
}else{

	 $solrium->getData(); 
}


  
?>