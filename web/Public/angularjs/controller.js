app.controller("DefaultController",function($scope,DvoydFactory){

$scope.products = {};
$scope.user = {};
$scope.search = "";
$scope.searched = false;
$scope.messaged = false;
$scope.imageUrl = "";
$scope.initialized = false;
$scope.limit = 10;



 DvoydFactory.getByLimit(3).then(function(data){
 	//if (data.data.length == 0) { alert("message?: DOMString")}
		$scope.products = data.data;
		$scope.searched = true;
		$scope.initialized = true;

		//$('.menutop').removeClass('fixed');

		$('.ui.search').search({source: data.data ,searchFields   : ['name','price','domaine']});

	},function(msg){})

$scope.nextResult =  function(limit){
	lmt = $scope.limit + limit;
	DvoydFactory.nextResult($scope.search,lmt).then(function(data){
		$scope.searched = true;
		$scope.products = data.data;
		//ui-semantic search
		$('.ui.search').search({source:  angular.fromJson(data.data) ,searchFields   : ['name','price','domaine']});

	},function(msg){ })
}
$scope.loadData = function(){
	DvoydFactory.getData($scope.search).then(function(data){
		$scope.searched = true;
		$scope.products = data.data;
		//ui-semantic search
		$('.ui.search').search({source:  angular.fromJson(data.data) ,searchFields   : ['name','price','domaine']});

	},function(msg){ })

}
$scope.sendEMail = function(){
	DvoydFactory.sendEMail($scope.user).then(function(data){
        $scope.messaged = true;
		$('.small').addClass("modal").modal('show');
		$scope.messaged = false;

	},function(msg){})
}


});