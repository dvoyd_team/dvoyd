app.factory("DvoydFactory",function($http,$q,$sce){

	var factory = {
		data : false,
		getData : function(d){
	        var deferred = $q.defer();
	        var promise = $http({
	        	method : "GET",
	        	url :url + "?q="+d,
	        	headers: {'Content-Type': "application/json"},
	          });
	        promise.then(function(data,status){
	         	factory.data =data;
	         	deferred.resolve(factory.data);
		        },function(data,status){
		         	deferred.reject("Error");
		        }
		    );
	        
	        return deferred.promise;
		},
		getByLimit : function(d){
	        var deferred = $q.defer();
	        var today = new Date();
	        var start =today.getSeconds() *today.getSeconds();
	        var promise = $http({
	        	method : "GET",
	        	url : url + "?limit="+d+"&start="+start,
	        	headers: {'Content-Type': "application/json"},
	        });

	        promise.then(function(data,status){
	         	factory.data =data;
	         	deferred.resolve(factory.data);
	        },(function(data,status){
	         	deferred.reject("Error");
	        }));
	        return deferred.promise;
		},
		nextResult : function(d,limit){
	        var deferred = $q.defer();
	        var today = new Date();
	        var start =today.getSeconds() *today.getSeconds();
	        var promise = $http({
	        	method : "GET",
	        	url : url + "?start="+limit+"&q="+d,
	        	headers: {'Content-Type': "application/json"},
	        });

	        promise.then(function(data,status){
	         	factory.data =data;
	         	deferred.resolve(factory.data);
	        },(function(data,status){
	         	deferred.reject("Error");
	        }));
	        return deferred.promise;
		},
		sendEMail : function(data){
			var deferred = $q.defer();
	        var database = firebase.database();
	        database.ref(" user "+ data.name).set({
	        	email : data.mail,
	        	name  : data.name,
	        	message : data.message,
	        	date    : new Date() 
	        }).then(function(data,status){
	        	deferred.resolve(status);
	        },function(error){
	        	deferred.reject(1);
	        })
	        return deferred.promise;
	       
		}
	}

return factory;
});


app.filter('urlFilter',function($http,$sce,$q){

	return function(url){
		var deferred = $q.defer();
		var promise = $http.get(ping+"?param="+url);
	    promise.then(function(data,status){
	     if(data.data == 0) return url;
	     console.log(data.data);
	     deferred.resolve(1);
	 },(function(data,status){ deferred.reject(0)}));
	    return deferred.promise;
	}
})