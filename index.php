<!DOCTYPE html>
<html ng-app="dvoyd">
<head>
   <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
   
    <link href="https://cdn.jsdelivr.net/semantic-ui/2.2.6/semantic.min.css" rel="stylesheet" />
    <!-- <link href="web/Public/dist/semantic.min.css" rel="stylesheet" /> -->
    <link href="web/Public/css/style.css" rel="stylesheet" />
	<title>Dvoyd - Votre comparateur de prix ecommerce</title>

	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80501443-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1805760883007053'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1805760883007053&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<body ng-controller="DefaultController as dy" style="background-color: rgb(212, 212, 212);">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.8&appId=746900492140097";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


 
      <div class="hero-text align-center top-header" >
         <h1 style="color:#f15b28" class="title">DVOYD</h1>
         <span class="title-features">votre comparateur de prix </span>
      </div>
 
 <div class="ui borderless main menu" style="background-color:#00aeef">
    <div class="ui text container">
      <div href="#" class="header item "></div>
      <div class="ui search search-bar">
      <div class="ui icon input  ">
         <div class="input-line">
            <form ng-submit="loadData()">
              <input class="form-input check-value" ng-model="search" ng-trim="true" ng-keyup="loadData()" placeholder="Que cherchez-vous..." type="text">
            </form>
             
          </div>
        <i class="search icon"></i>
      </div>
      <div class="results"></div>
    </div>
   
   </div>
  </div>
  <div class="container">
    <!-- <div class="ui segment loading" ng-show="!searched">
      <div class="ui active dimmer">
        <div class="ui text loader">Dvoyd</div>
      </div>
      <p></p>
    </div></div> -->
  <img ng-src="web/Public/reload.gif" alt="loading" class="loading" ng-show="!searched">
  
  <div class="ui  container  cards  main-view stackable  grid" ng-show="initialized">
     <div class="card" ng-repeat="product in products track by $index " >
       <div class="ui raised segment"><a class="ui ribbon {{ ($index % 2 == 0) ? 'teal' : 'brown'}}  label">Certains produits n'ont pas d'image ou Indisponible</a></div>
	    <div class="ui  image small bordered">
	        <img ng-src="{{product.image}}"    alt="{{product.name}}">
	    </div>
	  <div class="content">
	    <div class="header title-product">{{product.name}}</div>
	    <div class="meta">

	    <!--  <a>
              <div class="ui  labeled button" tabindex="0">
              <div class="ui red button"><i class="shop icon"></i>  FCFA </div>
              <a class="ui  {{ ($index % 2 == 0) ? 'teal' : 'brown'}} left pointing label">
               {{product.price | number}} FCFA
              </a> -->
           </div>
        
        </a>
	    </div>
	    <div class="description">
	     <a>
              <div class="ui labeled button" tabindex="0">
              <!-- <div class="ui red button"><i class="shop icon"></i>  FCFA </div> -->
              <a class="ui  {{ ($index % 2 == 0) ? 'teal right' : 'brown left'}}   label">
               {{product.price | number}} FCFA
              </a>
	    </div>
	  </div>
	  <div class="extra content">
	  <a class="ui  floated ui {{ ($index % 2 == 0) ? 'teal' : 'brown'}} right  label"> {{product.domaine}}</a>
        <a href="{{product.link}}" target="_blank"><span class=" floated ui red label">Voir detail</span></a>
      </div>
  </div>
</div>
<!-- <div class="container"> 
<a href="#" ng-click="nextResult(limit)" class="ui button green">Suivant</a>
</div>
 -->
<div class="ui top inverted teal fixed menu">

  <a class="item mobile-hide" href="https://www.facebook.com/dvoydstartup" target="_blank"><i class="facebook f icon"></i></a>
  <a class="item mobile-hide" href="https://plus.google.com/108481799623157687690" target="_blank"><i class="google plus icon"></i></a>
  <a class="item mobile-hide" href="https://twitter.com/dvoydstartup" target="_blank"><i class="twitter icon"></i></a> 
   <a class="item" id="modal">Nous contacter</i></a> 

  <a class="item"><div class="fb-like" data-href="https://www.facebook.com/dvoydstartup" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div></div></a> 
   
 <!-- <div class="fb-page" data-href="https://www.facebook.com/dvoydstartup/" data-tabs="timeline" data-width="180" data-height="70" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/dvoydstartup/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dvoydstartup/">Dvoyd</a></blockquote></div>  -->
 </div>


<div class="ui bottom fixed inverted  menu">
  <a class="item" id="modal">Nous contacter</i></a> 
  <a class="item mobile-hide"  style="color:#00aeef">dvoydstartup@gmail.com</a> 
  <a class="item  mobile-hide"  style="color:#00aeef">dvoyd@hotmail.fr</a>  
 </div>


<div class="ui small" ng-show="messaged" >
  <div class="header">Dvoyd, Votre comparateur de prix</div>
  <div class="content">
    <p>Merci pour votre message.  Merci d'aimer et de partager la page 
      <div class="fb-like" data-href="https://www.facebook.com/dvoydstartup" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div> </p>
  </div>
  <div class="actions">
    <div class="ui cancel red button">OK</div>
  </div>
</div>



  <div class="ui modal">
  <i class="close icon"></i>
  <div class="header">
    Dvoyd Contact
  </div>
  <div class="image content">
    <div class="ui medium image">
      <img src="web/Public/dvoyd_logo.png">
    </div>
    <div class="description">
      <div class="ui header">Renseigner ces champs</div>
      <p>
        <div class="ui  input focus"><input placeholder="name..." ng-model="user.name" type="text"></div>
        <div class="ui  input"><input placeholder="email..."      ng-model="user.mail" type="email"></div>
        <br>
        <div class="ui input fluid">
            <textarea class="ui message" ng-model="user.message" cols="43" rows="10" placeholder="content"> </textarea>
        </div>
      </p>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Annuler
    </div>
    <button class="ui positive right labeled icon button" ng-click="sendEMail()">
      Valider
      <i class="checkmark icon"></i>
    </button>
  </div>
</div>  


<script>
fbq('track', 'Search', {
search_string: 'leather sandals'
});
</script>

<script>
fbq('track', 'ViewContent', {
value: 3.50,
currency: 'USD'
});
</script>


<!--  <script src="web/Public/js/jquery.js"></script>--> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> 
<script src="https://cdn.jsdelivr.net/semantic-ui/2.2.6/semantic.min.js"></script>
<!-- <script src="web/Public/dist/semantic.min.js"></script>
 --><!--   <script src="web/Public/js/angular.js"></script>-->  
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.5/firebase.js"></script>
   <script src="web/Public/angularjs/app.js"></script>
  <script src="web/Public/angularjs/service.js"></script>
  <script src="web/Public/angularjs/controller.js"></script>
  <script src="web/Public/js/default_config.js"></script>

</body>
</html>